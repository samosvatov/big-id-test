const config = {
  root: true,
  env: {
    es6: true,
    node: true,
    browser: true,
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    sourceType: 'module',
    tsconfigRootDir: __dirname,
    ecmaFeatures: {
      jsx: true,
    },
    project: ['./tsconfig.json', './api-tsconfig.json', './app-tsconfig.json'],
  },

  plugins: ['@typescript-eslint', 'prettier', 'import', 'react', 'jsx-a11y'],

  extends: [
    'airbnb-typescript',
    'airbnb/hooks',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:eslint-comments/recommended',
    'prettier',
    'prettier/react',
    'prettier/@typescript-eslint',
  ],
  rules: {
    'import/prefer-default-export': 'off',
    'consistent-return': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-unused-vars': ['warn', { argsIgnorePattern: '^_' }],
    'react/require-default-props': [
      'error',
      { ignoreFunctionalComponents: true },
    ],
    'class-methods-use-this': 'off',
  },

  overrides: [
    {
      files: ['*.test.*'],
      rules: {
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/dot-notation': 'off',
      },
    },
  ],
};

module.exports = config;
