const config = {
  amqp: {
    host: process.env.AMQP_HOST || 'amqp://localhost',
    user: process.env.AMQP_USER || 'guest',
    password: process.env.AMQP_PASSWORD || 'guest',
    exchangeName: process.env.AMQP_EXCHANE_NAME || 'matching',
    topics: {
      matchingRequested: 'matching.requested',
    },
  },
};

export { config };
