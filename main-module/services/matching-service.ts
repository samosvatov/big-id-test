import LineByLineReader from 'line-by-line';
import { matchingRequestedPublisher } from '../amqp/publishers/matching-requested';

export class MatchingService {
  private lineOffset = 0;
  private publisher = matchingRequestedPublisher;

  private async matchLines(lines: any[], offset: number, isFinal: boolean) {
    await this.publisher.sendMessage({
      lines,
      lineOffset: offset,
      isFinal,
    });
  }

  public async readFile(filename: string): Promise<void> {
    const lines: string[] = [];
    const promises: Array<Promise<any>> = [];
    const lr = new LineByLineReader(filename);

    lr.on('line', (line) => {
      lr.pause();

      lines.push(line);

      if (lines.length === 1000) {
        const linesToProcess = lines.splice(0, 1000);
        promises.push(this.processLines(linesToProcess, false));
      }

      lr.resume();
    });

    lr.on('end', async () => {
      const linesToProcess = lines.splice(0, 1000);
      promises.push(this.processLines(linesToProcess, true));
      await Promise.all(promises);
    });
  }

  private async processLines(lines: string[], isFinal = false) {
    await this.matchLines(lines, this.lineOffset, isFinal);

    this.lineOffset += 1000;
  }

  public async start(filename: string) {
    await this.publisher.init();
    await this.readFile(filename);
  }
}

export default new MatchingService();
