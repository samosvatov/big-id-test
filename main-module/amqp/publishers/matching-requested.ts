import { BasePublisher } from 'common/amqp/base/base-publisher';
import { config } from 'main-module/config';
import { MatchingRequestedMessage } from '../../../common/interfaces';

export class MatchingRequestedPublisher extends BasePublisher<MatchingRequestedMessage> {
  constructor() {
    super({
      url: config.amqp.host,
      exchangeName: config.amqp.exchangeName,
      topicName: config.amqp.topics.matchingRequested,
    });
  }
}

export const matchingRequestedPublisher = new MatchingRequestedPublisher();
