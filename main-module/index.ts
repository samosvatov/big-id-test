import matchingService from './services/matching-service';

(async () => {
  try {
    await matchingService.start('./big.txt');
  } catch (e) {
    console.log(e);
  }
})();
