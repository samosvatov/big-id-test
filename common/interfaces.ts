export interface AmqpConfig {
  url: string;
  exchangeName: string;
  topicName: string;
}

export interface MatchingRequestedMessage {
  lines: string[];
  lineOffset: number;
  isFinal: boolean;
}

export interface MatchingResult {
  name: string;
  lineOffset: number;
  charOffset: number;
}

export interface MatchingFinishedMessage {
  entries: Array<MatchingResult>;
  isFinal: boolean;
}
