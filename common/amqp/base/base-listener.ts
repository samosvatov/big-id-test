import * as amqp from 'amqplib';
import { AmqpConfig } from '../../interfaces';

export abstract class BaseListener<Message> {
  readonly config: AmqpConfig;
  private channel: amqp.Channel;
  private connection: amqp.Connection;

  protected constructor(config: AmqpConfig) {
    this.config = config;
  }

  public abstract processMessage(message: Message): Promise<void> | void;

  private async handleMessage(message: amqp.Message) {
    const data = JSON.parse(message.content.toString());

    const channel = await this.getChannel();
    channel.ack(message);
    return this.processMessage(data);
  }

  public async init() {
    const channel = await this.getChannel();

    await channel.assertExchange(this.config.exchangeName, 'direct', {
      durable: true,
    });

    const { queue } = await channel.assertQueue(this.config.topicName, {
      durable: true,
    });

    await channel.bindQueue(queue, this.config.exchangeName, '');

    await channel.consume(queue, this.handleMessage.bind(this));
  }

  private async createConnection(): Promise<void> {
    this.connection = await amqp.connect(this.config.url);
  }

  private async getConnection(): Promise<amqp.Connection> {
    if (!this.connection) {
      await this.createConnection();
    }

    return this.connection;
  }

  private async createChannel(): Promise<void> {
    const connection = await this.getConnection();

    this.channel = await connection.createChannel();
  }

  private async getChannel(): Promise<amqp.Channel> {
    if (!this.channel) {
      await this.createChannel();
    }

    return this.channel;
  }
}
