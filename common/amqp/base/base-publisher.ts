import { AmqpConfig } from '../../interfaces';
import * as amqp from 'amqplib';

export class BasePublisher<Message> {
  readonly config: AmqpConfig;
  private channel: amqp.Channel;
  private connection: amqp.Connection;

  constructor(config: AmqpConfig) {
    this.config = config;
  }
  private async createConnection(): Promise<void> {
    this.connection = await amqp.connect(this.config.url);
  }

  private async getConnection(): Promise<amqp.Connection> {
    if (!this.connection) {
      await this.createConnection();
    }

    return this.connection;
  }

  private async createChannel(): Promise<void> {
    const connection = await this.getConnection();

    this.channel = await connection.createChannel();
  }

  private async getChannel(): Promise<amqp.Channel> {
    if (!this.channel) {
      await this.createChannel();
    }

    return this.channel;
  }

  public async init() {
    const channel = await this.getChannel();

    await channel.assertExchange(this.config.exchangeName, 'direct', {
      durable: true,
    });

    await channel.assertQueue(this.config.topicName);
  }

  public async sendMessage(message: Message) {
    const channel = await this.getChannel();

    await channel.sendToQueue(
      this.config.topicName,
      Buffer.from(JSON.stringify(message))
    );
  }
}
