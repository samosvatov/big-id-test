import * as amqp from 'amqplib';
import { AmqpConfig } from '../interfaces';

export class Connection {
  private config: AmqpConfig;
  private channel: amqp.Channel;
  private connection: amqp.Connection;

  constructor(config: AmqpConfig) {
    this.config = config;
  }

  public async closeConnection() {
    return this.connection.close();
  }
}
