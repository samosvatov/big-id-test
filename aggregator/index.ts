import { matchingFinishedListener } from './amqp/listeners/matching-finished';

(async () => {
  await matchingFinishedListener.init();
})();
