import { BaseListener } from 'common/amqp/base';
import { config } from '../../config';
import {
  aggregatorHandler,
  AggregatorHandler,
} from '../../handlers/aggregator';
import { MatchingFinishedMessage } from '../../../common/interfaces';

export class MatchingFinishedListener extends BaseListener<MatchingFinishedMessage> {
  private handler: AggregatorHandler;

  constructor() {
    super({
      url: config.amqp.host,
      exchangeName: config.amqp.exchangeName,
      topicName: config.amqp.topics.matchingFinished,
    });

    this.handler = aggregatorHandler;
  }

  public processMessage(message: MatchingFinishedMessage) {
    return this.handler.handleFinished(message);
  }
}

export const matchingFinishedListener = new MatchingFinishedListener();
