import { MatchingFinishedMessage } from '../../common/interfaces';

interface NameEntry {
  lineOffset: number;
  charOffset: number;
}

export class AggregatorHandler {
  private entries: { [key: string]: NameEntry[] } = {};

  public handleFinished(message: MatchingFinishedMessage) {
    if (!message.entries.length && !message.isFinal) {
      return;
    }

    message.entries.map((entry) => {
      const nameEntry = {
        lineOffset: entry.lineOffset,
        charOffset: entry.charOffset,
      };
      if (!this.entries[entry.name] || !this.entries[entry.name].length) {
        this.entries[entry.name] = [];
      }
      this.entries[entry.name].push(nameEntry);
    });

    if (message.isFinal) {
      console.log('RESULT:', this.entries);
      this.entries = {};
    }
  }
}

export const aggregatorHandler = new AggregatorHandler();
