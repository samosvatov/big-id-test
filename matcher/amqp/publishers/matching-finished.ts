import { BasePublisher } from 'common/amqp/base/base-publisher';
import { config } from '../../config';
import { MatchingFinishedMessage } from '../../../common/interfaces';

export class MatchingFinishedPublisher extends BasePublisher<MatchingFinishedMessage> {
  constructor() {
    super({
      url: config.amqp.host,
      exchangeName: config.amqp.exchangeName,
      topicName: config.amqp.topics.matchingFinished,
    });
  }
}

export const matchingFinishedPublisher = new MatchingFinishedPublisher();
