import { BaseListener } from 'common/amqp/base';
import { config } from '../../config';
import { MatchHandler } from '../../handlers/match';
import { MatchingRequestedMessage } from '../../../common/interfaces';

export class MatchingRequestedListener extends BaseListener<MatchingRequestedMessage> {
  private handler: MatchHandler;

  constructor() {
    super({
      url: config.amqp.host,
      exchangeName: config.amqp.exchangeName,
      topicName: config.amqp.topics.matchingRequested,
    });

    this.handler = new MatchHandler();
  }

  public async processMessage(
    message: MatchingRequestedMessage
  ): Promise<void> {
    const { lines, lineOffset, isFinal } = message;

    return this.handler.processLines(lines, lineOffset, isFinal);
  }
}
