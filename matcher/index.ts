import { MatchingRequestedListener } from './amqp/listeners/matching-requested';

(async () => {
  const matchingRequestedListener = new MatchingRequestedListener();
  await matchingRequestedListener.init();
})();
