import { config } from '../config';
import { matchingFinishedPublisher } from '../amqp/publishers/matching-finished';
import { MatchingResult } from '../../common/interfaces';

export class MatchHandler {
  private publisher = matchingFinishedPublisher;

  private countNameOccurrences(line: string, name: string): number[] {
    const charOffsets: number[] = [];
    const lowLine = line.toLowerCase().split(' ');
    const lowName = name.toLowerCase().replace(/\s+/g, '');

    let index = lowLine.indexOf(lowName);

    while (index !== -1) {
      charOffsets.push(index);
      index = lowLine.indexOf(lowName, index + 1);
    }

    return charOffsets;
  }

  private processLine(line: string, lineOffset: number): MatchingResult[] {
    let accum: MatchingResult[] = [];
    if (line === '') {
      return accum;
    }

    config.names.map((name) => {
      const charOffsets = this.countNameOccurrences(line, name);
      if (charOffsets.length > 0) {
        const formatedRecords = charOffsets.map((charOffset) => ({
          lineOffset,
          charOffset,
          name,
        }));

        accum = accum.concat(formatedRecords);
      }
    });

    return accum;
  }

  public async processLines(
    lines: string[],
    linesOffset: number,
    isFinal: boolean
  ) {
    await this.publisher.init();

    let accum: MatchingResult[] = [];

    lines.map((line, index) => {
      const processedLine = this.processLine(line, linesOffset + index + 1);
      if (processedLine.length > 0) {
        accum = accum.concat(processedLine);
      }
    });

    if (accum.length > 0 || isFinal) {
      await this.publisher.sendMessage({ entries: accum, isFinal });
    }
  }
}
